package myapp.dao;

import myapp.model.User;

import java.util.List;

/**
 * Created by julius on 08.04.2017.
 */
public interface UserManager {
    public void add(User user);
    public void removeById(int userId);
    public void printUsers();
    public User getByLogin(String logName);
    public User getById(int userId);
    public List<User> getUsersByList();
    public User SignIn(String logName, String password);
}
