package myapp.dao;

import myapp.Utils.HibernateUtil;
import myapp.model.User;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;

import java.util.Iterator;
import java.util.List;

/**
 * Created by julius on 06.04.2017.
 */
public class UserManagerImpl implements UserManager {


    public void add(User user) {
        SessionFactory factory = HibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            if (getByLogin(user.getLogName())==null)
                session.save(user);
            else
                System.out.println("We have user with this login");
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void removeById(int userId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User user = (User) session.get(User.class, userId);
            if (user != null) {
                session.delete(user);
                tx.commit();
                System.out.println("User was deleted");
            } else
                System.out.println("cant find user");
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            //e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void printUsers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<User> users = session.createQuery("FROM User").list();
            for (Iterator iterator = users.iterator(); iterator.hasNext(); ) {
                User user = (User) iterator.next();
                System.out.print("First Name: " + user.getFirstName());
                System.out.println("  Last Name: " + user.getLastName());
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<User> getUsersByList() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<User> users = session.createQuery("FROM User").list();
            tx.commit();
            return users;
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return null;
    }


    public User getByLogin(String logName) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("logName", logName));
            List<User> users = criteria.list();
            if (users.size() > 0 && users != null)
                user = users.get(0);
            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }


    public User SignIn(String logName, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        User user = null;
        try {
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("logName", logName));
            criteria.add(Restrictions.eq("password", password));
            List<User> users = criteria.list();
            if (users.size() > 0 && users != null)
                user = users.get(0);
            tx.commit();

        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }



    public User getById(int userId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            User user = (User) session.get(User.class, userId);
            tx.commit();
            return user;
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
}
