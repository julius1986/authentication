package myapp.app.Console.controller;

import myapp.Service.AdminService;
import myapp.Service.AdminServiceImpl;
import myapp.model.User;

import java.util.Scanner;

/**
 * Created by julius on 19.04.2017.
 */
public class AdminController {
    AdminService adminService = new AdminServiceImpl();
    User user;

    public AdminController(User user) {
        this.user = user;
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        String message;
        System.out.println("1 - show users");
        System.out.println("2 - add user");
        System.out.println("0 - logout");
        while (scanner.hasNext()) {
            message = scanner.next();
            scanner.nextLine();
            if (message.equals("0")) {
                adminService = null;
                break;
            }
            if (message.equals("1")) {
                adminService.printUsers();
            }
            if (message.equals("2")) {
                User user = creatUser();
                if(user!=null)
                    adminService.add(user);
                else
                    System.out.println("We have error to create user");
            }
            System.out.println("1 - show users");
            System.out.println("2 - add user");
            System.out.println("0 - logout");

        }


    }

    private User creatUser(){
        Scanner scanner = new Scanner(System.in);
        String firstname, lastname, login, password, position;
        System.out.println("Insert First name");
        firstname = scanner.next();
        scanner.nextLine();
        System.out.println("Insert Last name");
        lastname = scanner.next();
        scanner.nextLine();
        System.out.println("Insert Login");
        login = scanner.next();
        scanner.nextLine();
        System.out.println("Insert Password");
        password = scanner.next();
        scanner.nextLine();
        System.out.println("Choose position");
        System.out.println("0 - Admin   1 - User");
        int pos;
        try {
            pos = Integer.parseInt(scanner.next());
        } catch (Exception e){
            System.out.println("its not a number");
            return null;
        }
        if (pos==0)
            position = "Admin";
        else if (pos==1)
            position = "User";
        else{
            scanner.nextLine();
            return null;
        }
        scanner.nextLine();
        return new User(firstname, lastname, login, password, position);
    }

}

