package myapp.app.Console;

import myapp.Utils.HibernateUtil;
import myapp.app.Application;
import myapp.app.Console.controller.AdminController;
import myapp.app.Console.controller.UserController;
import myapp.dao.UserManager;
import myapp.dao.UserManagerImpl;
import myapp.model.User;

import java.util.Scanner;

/**
 * Created by julius on 19.04.2017.
 */
public class ConsoleApp implements Application {

    public ConsoleApp() {

    }

    public void start() {
        HibernateUtil hibernateUtil = new HibernateUtil();
        UserManager userManager = new UserManagerImpl();
        userManager.add(new User("Vasy", "Pupkin", "User", "User", "User"));
        userManager.add(new User("Igor", "Pavlov", "Admin", "Admin", "Admin"));
        userManager.add(new User("Petya", "Valenkov", "maler", "epichardpassword1", "User"));
        userManager.add(new User("Petya", "Valenkov", "maler2", "111", "User"));

        //userManager.printUsers();
        //userManager.printUsers();
        User currentUser;
        Scanner scanner = new Scanner(System.in);
        String message;
        String password;
        String login;
        System.out.println("Pls write login and password(example: Mylogin   MyPassword)");
        System.out.println("Or type exit for close programm");
        while (scanner.hasNext()) {
            login = scanner.next();
            if (login.equals("exit"))
                break;
            else {
                password = scanner.next();
                currentUser = userManager.SignIn(login, password);
                if (currentUser != null) {
                    if(currentUser.getPosition().equals("Admin"))
                    {
                        new AdminController(currentUser).start();
                    }
                    else if(currentUser.getPosition().equals("User"))
                    {
                        new UserController(currentUser).start();
                    }
                    else{
                        System.out.println("Cant find position of user");
                    }
                }
                else {
                    System.out.println("Wrong user or password");
                }
            }
            System.out.println("Pls write login and password(example: Mylogin   MyPassword)");
            System.out.println("Or type exit for close programm");
            scanner.nextLine();
            password = "";
            message = "";
        }


    }


}
