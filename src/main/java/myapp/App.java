package myapp;


import myapp.app.Application;
import myapp.app.Console.ConsoleApp;

public class App {
    public static void main(String[] args) {
        Application consoleApp = new ConsoleApp();
        consoleApp.start();

    }
}
