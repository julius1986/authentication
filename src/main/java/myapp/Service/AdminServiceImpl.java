package myapp.Service;

import myapp.dao.UserManager;
import myapp.dao.UserManagerImpl;
import myapp.model.User;

/**
 * Created by julius on 08.04.2017.
 */
public class AdminServiceImpl implements AdminService {
    UserManager userManager = new UserManagerImpl();
    public void add(User user) {
        userManager.add(user);
    }

    public void removeById(int userId) {
        userManager.removeById(userId);
    }

    public void printUsers() {
        userManager.printUsers();
    }


    public User getByLogin(String logName) {
        return userManager.getByLogin(logName);
    }

}
