package myapp.Service;

import myapp.model.User;

/**
 * Created by julius on 08.04.2017.
 */
public interface AdminService {
    public void printUsers();
    public void add(User user);
    public void removeById(int userId);
    public User getByLogin(String logName);
}
